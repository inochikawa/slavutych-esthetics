using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Mvc;
using System.Security.Claims;
using SE.DataAccess.Identity;
using SE.Model;
using SE.Web.ViewModels.Account;
using Claim = System.Security.Claims.Claim;

namespace SlavEst.WebCore.Controllers
{
    public class AccountController : Controller
    {
        private SeIdentityContext db;

        public AccountController(SeIdentityContext context)
        {
            db = context;
        }

        [HttpGet]
        public IActionResult Login()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Login(LoginViewModel model)
        {
            if (ModelState.IsValid)
            {
                SeUser user = await db.Users.FirstOrDefaultAsync(u => u.Email == model.Email && u.Password == model.Password);
                if (user != null)
                {
                    await Authenticate(model.Email); // ��������������

                    return RedirectToAction("Index", "Home");
                }
                ModelState.AddModelError("", "������������ ����� �(���) ������");
            }
            return View(model);
        }

        [HttpGet]
        public IActionResult Register()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Register(RegisterViewModel model)
        {
            if (ModelState.IsValid)
            {
                SeUser user = await db.Users.FirstOrDefaultAsync(u => u.Email == model.Email);
                if (user == null)
                {
                    // ��������� ������������ � ��
                    db.Users.Add(new SeUser { Email = model.Email, Password = model.Password, Name = model.Name, Surname = model.Surname});
                    await db.SaveChangesAsync();

                    await Authenticate(model.Email); // ��������������

                    return RedirectToAction("Index", "Home");
                }
                else
                    ModelState.AddModelError("", "������������ ����� �(���) ������");
            }
            return View(model);
        }

        public async Task<IActionResult> Logout()
        {
            await HttpContext.Authentication.SignOutAsync("Cookies");
            return RedirectToAction("Login", "Account");
        }

        private async Task Authenticate(string userName)
        {
            // ������� ���� claim
            var claims = new List<Claim>
                    {
                        new Claim(ClaimsIdentity.DefaultNameClaimType, userName)
                    };
            // ������� ������ ClaimsIdentity
            ClaimsIdentity id = new ClaimsIdentity(claims, "ApplicationCookie", ClaimsIdentity.DefaultNameClaimType,
                ClaimsIdentity.DefaultRoleClaimType);
            // ��������� ������������������ ����
            await HttpContext.Authentication.SignInAsync("Cookies", new ClaimsPrincipal(id));
        }
    }
}