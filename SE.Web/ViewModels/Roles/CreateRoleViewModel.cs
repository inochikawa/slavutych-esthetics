﻿namespace SE.Web.ViewModels.Roles
{
    public class CreateRoleViewModel
    {
        public string Name { get; set; }
        public string Description { get; set; }
    }
}