﻿using Microsoft.EntityFrameworkCore;
using SE.Model;

namespace SE.DataAccess.Identity
{
    public class SeIdentityContext : DbContext
    {
        public DbSet<SeUser> Users { get; set; }
        public DbSet<SeRole> Roles { get; set; }

        public SeIdentityContext(DbContextOptions<SeIdentityContext> options)
            : base(options)
        {
        }
    }
}