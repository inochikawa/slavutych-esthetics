﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using SE.DataAccess;
using SE.Model.Enums;

namespace SE.DataAccess.Migrations
{
    [DbContext(typeof(SeContext))]
    [Migration("20170329004005_Init2")]
    partial class Init2
    {
        protected override void BuildTargetModel(ModelBuilder modelBuilder)
        {
            modelBuilder
                .HasAnnotation("ProductVersion", "1.1.1")
                .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

            modelBuilder.Entity("SE.Model.BuildingType", b =>
                {
                    b.Property<Guid>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("TypeName");

                    b.HasKey("Id");

                    b.ToTable("BuildingTypes");
                });

            modelBuilder.Entity("SE.Model.Claim", b =>
                {
                    b.Property<Guid>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<Guid?>("AddressId");

                    b.Property<string>("Description");

                    b.Property<int>("DifficultyLevel");

                    b.Property<Guid?>("ExecutionStatusId");

                    b.Property<DateTime>("PublishDate");

                    b.Property<string>("PublisherName");

                    b.Property<Guid?>("RatingId");

                    b.Property<string>("ShortDescription");

                    b.Property<string>("Title");

                    b.HasKey("Id");

                    b.HasIndex("AddressId");

                    b.HasIndex("ExecutionStatusId");

                    b.HasIndex("RatingId");

                    b.ToTable("Claims");
                });

            modelBuilder.Entity("SE.Model.ClaimAddress", b =>
                {
                    b.Property<Guid>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<Guid?>("BuildingTypeId");

                    b.Property<string>("StreetName");

                    b.Property<int>("StreetNumber");

                    b.HasKey("Id");

                    b.HasIndex("BuildingTypeId");

                    b.ToTable("ClaimAddresses");
                });

            modelBuilder.Entity("SE.Model.ClaimAttachment", b =>
                {
                    b.Property<Guid>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<Guid>("ClaimId");

                    b.Property<string>("Url");

                    b.HasKey("Id");

                    b.HasIndex("ClaimId");

                    b.ToTable("ClaimAttachments");
                });

            modelBuilder.Entity("SE.Model.Classification", b =>
                {
                    b.Property<Guid>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<Guid?>("ClaimId");

                    b.Property<string>("Name");

                    b.Property<Guid?>("TeamId");

                    b.HasKey("Id");

                    b.HasIndex("ClaimId");

                    b.HasIndex("TeamId");

                    b.ToTable("Classifications");
                });

            modelBuilder.Entity("SE.Model.Emplyee", b =>
                {
                    b.Property<Guid>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("Discriminator")
                        .IsRequired();

                    b.Property<Guid>("PositionId");

                    b.Property<Guid>("UserId");

                    b.HasKey("Id");

                    b.HasIndex("PositionId");

                    b.HasIndex("UserId");

                    b.ToTable("Emplyees");

                    b.HasDiscriminator<string>("Discriminator").HasValue("Emplyee");
                });

            modelBuilder.Entity("SE.Model.ExecutionStatus", b =>
                {
                    b.Property<Guid>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("Comment");

                    b.Property<int>("Type");

                    b.HasKey("Id");

                    b.ToTable("ExecutionStatuses");
                });

            modelBuilder.Entity("SE.Model.PermanentAddress", b =>
                {
                    b.Property<Guid>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<Guid?>("BuildingTypeId");

                    b.Property<int>("BuildsCount");

                    b.Property<string>("StreetName");

                    b.HasKey("Id");

                    b.HasIndex("BuildingTypeId");

                    b.ToTable("PermanentAddresses");
                });

            modelBuilder.Entity("SE.Model.Position", b =>
                {
                    b.Property<Guid>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("Name")
                        .IsRequired();

                    b.HasKey("Id");

                    b.ToTable("Positions");
                });

            modelBuilder.Entity("SE.Model.Rating", b =>
                {
                    b.Property<Guid>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<int>("Mark");

                    b.Property<Guid>("TargetId");

                    b.HasKey("Id");

                    b.ToTable("Ratings");
                });

            modelBuilder.Entity("SE.Model.SeRole", b =>
                {
                    b.Property<Guid>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("Description");

                    b.Property<string>("Name");

                    b.HasKey("Id");

                    b.ToTable("SeRole");
                });

            modelBuilder.Entity("SE.Model.SeUser", b =>
                {
                    b.Property<Guid>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("Email")
                        .IsRequired();

                    b.Property<string>("Name")
                        .IsRequired();

                    b.Property<string>("Password")
                        .IsRequired();

                    b.Property<string>("PhoneNumber");

                    b.Property<Guid?>("RoleId");

                    b.Property<string>("Surname")
                        .IsRequired();

                    b.HasKey("Id");

                    b.HasIndex("RoleId");

                    b.ToTable("SeUser");
                });

            modelBuilder.Entity("SE.Model.Team", b =>
                {
                    b.Property<Guid>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("Name")
                        .IsRequired();

                    b.Property<Guid>("TeamleadId");

                    b.HasKey("Id");

                    b.HasIndex("TeamleadId")
                        .IsUnique();

                    b.ToTable("Teams");
                });

            modelBuilder.Entity("SE.Model.Teamlead", b =>
                {
                    b.Property<Guid>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<Guid>("TeamId");

                    b.Property<Guid?>("UserId");

                    b.HasKey("Id");

                    b.HasIndex("UserId");

                    b.ToTable("Teamleads");
                });

            modelBuilder.Entity("SE.Model.CoWorker", b =>
                {
                    b.HasBaseType("SE.Model.Emplyee");

                    b.Property<Guid?>("TeamId");

                    b.HasIndex("TeamId");

                    b.ToTable("CoWorkers");

                    b.HasDiscriminator().HasValue("CoWorker");
                });

            modelBuilder.Entity("SE.Model.Volunteer", b =>
                {
                    b.HasBaseType("SE.Model.Emplyee");


                    b.ToTable("Volunteers");

                    b.HasDiscriminator().HasValue("Volunteer");
                });

            modelBuilder.Entity("SE.Model.Claim", b =>
                {
                    b.HasOne("SE.Model.ClaimAddress", "Address")
                        .WithMany()
                        .HasForeignKey("AddressId");

                    b.HasOne("SE.Model.ExecutionStatus", "ExecutionStatus")
                        .WithMany()
                        .HasForeignKey("ExecutionStatusId");

                    b.HasOne("SE.Model.Rating", "Rating")
                        .WithMany()
                        .HasForeignKey("RatingId");
                });

            modelBuilder.Entity("SE.Model.ClaimAddress", b =>
                {
                    b.HasOne("SE.Model.BuildingType", "BuildingType")
                        .WithMany()
                        .HasForeignKey("BuildingTypeId");
                });

            modelBuilder.Entity("SE.Model.ClaimAttachment", b =>
                {
                    b.HasOne("SE.Model.Claim", "Claim")
                        .WithMany("Attachments")
                        .HasForeignKey("ClaimId")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("SE.Model.Classification", b =>
                {
                    b.HasOne("SE.Model.Claim")
                        .WithMany("Classifications")
                        .HasForeignKey("ClaimId");

                    b.HasOne("SE.Model.Team")
                        .WithMany("Classifications")
                        .HasForeignKey("TeamId");
                });

            modelBuilder.Entity("SE.Model.Emplyee", b =>
                {
                    b.HasOne("SE.Model.Position", "Position")
                        .WithMany()
                        .HasForeignKey("PositionId")
                        .OnDelete(DeleteBehavior.Cascade);

                    b.HasOne("SE.Model.SeUser", "User")
                        .WithMany()
                        .HasForeignKey("UserId")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("SE.Model.PermanentAddress", b =>
                {
                    b.HasOne("SE.Model.BuildingType", "BuildingType")
                        .WithMany()
                        .HasForeignKey("BuildingTypeId");
                });

            modelBuilder.Entity("SE.Model.SeUser", b =>
                {
                    b.HasOne("SE.Model.SeRole", "Role")
                        .WithMany("Users")
                        .HasForeignKey("RoleId");
                });

            modelBuilder.Entity("SE.Model.Team", b =>
                {
                    b.HasOne("SE.Model.Teamlead", "Teamlead")
                        .WithOne("Team")
                        .HasForeignKey("SE.Model.Team", "TeamleadId")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("SE.Model.Teamlead", b =>
                {
                    b.HasOne("SE.Model.SeUser", "User")
                        .WithMany()
                        .HasForeignKey("UserId");
                });

            modelBuilder.Entity("SE.Model.CoWorker", b =>
                {
                    b.HasOne("SE.Model.Team", "Team")
                        .WithMany("CoWorkers")
                        .HasForeignKey("TeamId");
                });
        }
    }
}
