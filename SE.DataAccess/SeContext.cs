﻿using Microsoft.EntityFrameworkCore;
using SE.Model;

namespace SE.DataAccess
{
    public class SeContext : DbContext
    {
        public DbSet<BuildingType> BuildingTypes { get; set; }
        public DbSet<Claim> Claims { get; set; }
        public DbSet<ClaimAddress> ClaimAddresses { get; set; }
        public DbSet<ClaimAttachment> ClaimAttachments { get; set; }
        public DbSet<Classification> Classifications { get; set; }
        public DbSet<CoWorker> CoWorkers { get; set; }
        public DbSet<Emplyee> Emplyees { get; set; }
        public DbSet<ExecutionStatus> ExecutionStatuses { get; set; }
        public DbSet<PermanentAddress> PermanentAddresses { get; set; }
        public DbSet<Position> Positions { get; set; }
        public DbSet<Rating> Ratings { get; set; }
        public DbSet<Team> Teams { get; set; }
        public DbSet<Teamlead> Teamleads { get; set; }
        public DbSet<Volunteer> Volunteers { get; set; }

        public SeContext(DbContextOptions<SeContext> options)
            : base(options)
        {
        }
    }
}