﻿using System;
using SE.Model.Enums;

namespace SE.Model
{
    public class ExecutionStatus
    {
        public Guid Id { get; set;}
        public StatusType Type { get; set; }
        public string Comment { get; set; }
    }
}
