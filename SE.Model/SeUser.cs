﻿using System;
using System.ComponentModel.DataAnnotations;

namespace SE.Model
{
    public class SeUser
    {
        public Guid Id { get; set; }
        [Required]
        public string Name { get; set; }
        [Required]
        public string Surname { get; set; }
        [Required]
        public string Email { get; set; }
        [Required]
        public string Password { get; set; }
        public string PhoneNumber { get; set; }
        public SeRole Role { get; set; }
    }
}