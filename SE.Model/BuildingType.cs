﻿using System;

namespace SE.Model
{
    public class BuildingType
    {
        public Guid Id { get; set; }
        public string TypeName { get; set; }
    }
}