﻿namespace SE.Model.Enums
{
    public enum StatusType
    {
        NotViewed = 0, 
        Viewed = 1,
        InProcess = 2,
        Done = 3,
        Canceled = 4
    }
}