﻿namespace SE.Model.Enums
{
    public enum DifficultyLevel
    {
        Easy = 1,
        Hard = 2
    }
}
