﻿using System;

namespace SE.Model
{
    public class PermanentAddress
    {
        public Guid Id { get; set; }
        public string StreetName { get; set; }
        public int BuildsCount { get; set; }
        public BuildingType BuildingType { get; set; }
    }
}
