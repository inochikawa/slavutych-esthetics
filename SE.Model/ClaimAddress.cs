﻿using System;

namespace SE.Model
{
    public class ClaimAddress
    {
        public Guid Id { get; set; }
        public string StreetName { get; set; }
        public int StreetNumber { get; set; }
        public BuildingType BuildingType { get; set; }
    }
}
