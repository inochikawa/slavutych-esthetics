﻿using System;

namespace SE.Model
{
    public class Rating
    {
        public Guid Id { get; set; }
        public Guid TargetId { get; set; }
        public int Mark { get; set; }
    }
}
