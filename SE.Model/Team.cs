﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace SE.Model
{
    [Table("Teams")]
    public class Team
    {
        [Key]
        public Guid Id { get; set; }
        public Guid TeamleadId { get; set; }
        [Required]
        public string Name { get; set; }

        [Required]
        [ForeignKey("TeamleadId")]
        public Teamlead Teamlead { get; set; }
        public ICollection<Classification> Classifications { get; set; }
        public ICollection<CoWorker> CoWorkers { get; set; }
    }
}