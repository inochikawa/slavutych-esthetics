﻿using System;

namespace SE.Model
{
    public class ClaimAttachment
    {
        public Guid Id { get; set; }
        public Guid ClaimId { get; set; }
        public string Url { get; set; }
        public Claim Claim { get; set; }
    }
}
