﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using SE.Model.Enums;

namespace SE.Model
{
    public class Claim
    {
        public Guid Id { get; set; }
        public string Title { get; set; }

        [MinLength(10, ErrorMessage = @"Опис занадто короткий")]
        public string ShortDescription { get; set; }

        [MinLength(20, ErrorMessage = @"Опис занадто короткий")]
        public string Description { get; set; }
        public DateTime PublishDate { get; set; }
        
        public string PublisherName { get; set; }
        public Rating Rating { get; set; }
        public ExecutionStatus ExecutionStatus { get; set; }
        public ClaimAddress Address { get; set; }
        public ICollection<ClaimAttachment> Attachments { get; set; }
        public ICollection<Classification> Classifications { get; set; }
        public DifficultyLevel DifficultyLevel { get; set; }
    }
}
