﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace SE.Model
{
    [Table("Classifications")]
    public class Classification
    {
        [Key]
        public Guid Id { get; set; }
        public string Name { get; set; }
    }
}