﻿using System;
using System.Collections.Generic;

namespace SE.Model
{
    public class SeRole
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public ICollection<SeUser> Users { get; set; }
    }
}