﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace SE.Model
{
    [NotMapped]
    public class Emplyee
    {
        [Key]
        public Guid Id { get; set; }
        [Required]
        public SeUser User { get; set; }
        [Required]
        public Position Position { get; set; }
    }
}
