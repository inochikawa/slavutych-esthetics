﻿using System.ComponentModel.DataAnnotations.Schema;

namespace SE.Model
{
    [Table("CoWorkers")]
    public class CoWorker : Emplyee
    {
        public Team Team { get; set; }
    }
}
