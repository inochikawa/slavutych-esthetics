﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace SE.Model
{
    [Table("Teamleads")]
    public class Teamlead
    {
        [Key]
        public Guid Id { get; set; }
        public SeUser User { get; set; }
        public Team Team { get; set; }
    }
}